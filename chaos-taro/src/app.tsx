import 'taro-ui/dist/style/index.scss'
import '@/styles/theme.scss'
import React from 'react'

const App: React.FC = ({ children }) => {
  return (
    <>
      {children}
    </>
  )
}

export default App;
